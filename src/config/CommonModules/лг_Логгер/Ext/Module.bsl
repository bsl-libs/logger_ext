﻿// Создание объекта логгера. Логгер - объект настроек логирования для
// 	заданной аналитики Объекта/События/Региона
//  

#Область ПрограммныйИнтерфейс

// Возвращает объект логгера. Вызывает сервер
//
// Параметры: 
// 	ОбъектИмя - Строка - Имя объекта логирования
// 	СобытиеИмя - Строка - Имя события логирования
// 	Регион - Строка - Регион использования адаптеров
//
// Возвращаемое значение: 
// 	Структура
//
Функция Создать(ОбъектИмя, СобытиеИмя, Регион = Неопределено) Экспорт
	
	Результат	= Новый Структура(лг_ЛоггерПовтИсп.Создать(Регион));
	
	Объект(Результат, ОбъектИмя);
	Событие(Результат, СобытиеИмя);
	
	Возврат Результат;
	
КонецФункции // Создать 

// Устанавливает имя объекта логирования. Возвращает значение на момент до выполнения метода
//
// Параметры: 
// 	Логгер - Структура - Описание логгера
// 	Значение - Строка - Имя объекта логирования
//
// Возвращаемое значение: 
// 	Строка
//
Функция Объект(Логгер, Значение = Null) Экспорт
	
	Возврат лг_Модуль.ом_Коллекция().СтруктураСвойство(Логгер, "Объект", Значение);
	
КонецФункции // Объект 

// Устанавливает имя события логирования. Возвращает значение на момент до выполнения метода
//
// Параметры: 
// 	Логгер - Структура - Описание логгера
// 	Значение - Строка - Имя события логирования
//
// Возвращаемое значение: 
// 	Строка
//
Функция Событие(Логгер, Значение = Null) Экспорт
	
	Возврат лг_Модуль.ом_Коллекция().СтруктураСвойство(Логгер, "Событие", Значение);
	
КонецФункции // Событие 

// Устанавливает уровень ведения лога. Возвращает значение на момент до выполнения метода
//
// Параметры: 
// 	Логгер - Структура - Описание логгера
// 	Значение - Число - Уровень лога по лг_Уровень
//
// Возвращаемое значение: 
// 	Число
//
Функция Уровень(Логгер, Значение = Null) Экспорт
	
	Возврат лг_Модуль.ом_Значение().Свойство(Логгер, "Уровень", Значение);
	
КонецФункции // Уровень 

// Устанавливает флаг ведения лога. Возвращает значение на момент до выполнения метода
//
// Параметры: 
// 	Логгер - Структура - Описание логгера
// 	Значение - Булево - Новое значение флага
//
// Возвращаемое значение: 
// 	Булево
//
Функция Включен(Логгер, Значение = Null) Экспорт
	
	Возврат лг_Модуль.ом_Коллекция().СтруктураСвойство(Логгер, "Включен", Значение);
	
КонецФункции // Включен 

// Устанавливает шаблон записи лога (лг_Шаблон). Возвращает значение на момент до выполнения метода
//
// Параметры: 
// 	Логгер - Структура - Описание логгера
// 	Значение - Строка - Шаблон записи
//
// Возвращаемое значение: 
// 	Строка
//
Функция Шаблон(Логгер, Значение = Null) Экспорт
	
	Возврат лг_Модуль.ом_Коллекция().СтруктураСвойство(Логгер, "Шаблон", Значение);
	
КонецФункции // Шаблон 

// Устанавливает константные сеансовые значения в логгере. Возвращает значение на момент до выполнения метода
//
// Параметры: 
// 	Логгер - Структура - Описание логгера
// 	Значение - ФиксированнаяСтруктура - Новое значение
//
// Возвращаемое значение: 
// 	ФиксированнаяСтруктура
//
Функция Константы(Логгер, Значение = Null) Экспорт
	
	Возврат лг_Модуль.ом_Коллекция().СтруктураСвойство(Логгер, "Константы", Значение);
	
КонецФункции // Константы 

#КонецОбласти

#Область СлужебныйПрограммныйИнтерфейс

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

#КонецОбласти
