﻿// Кэш настроек логирования
//  

#Область СлужебныйПрограммныйИнтерфейс

// Возвращает настройки логирования
//
// Параметры: 
// 	Регион - Строка - Регион использования адаптеров
//
// Возвращаемое значение: 
// 	Структура
//
Функция Получить(Регион = Неопределено) Экспорт
	
	Возврат Новый ФиксированнаяСтруктура(лг_НастройкаВызовСервера.Получить(Регион));
	
КонецФункции // Получить 

// Возвращает настройку по-умолчанию
//
// Параметры: 
// 	Регион - Строка - Регион использования адаптеров
//
// Возвращаемое значение: 
// 	Структура
//
Функция Умолчание(Регион = Неопределено) Экспорт
	
	Возврат Новый ФиксированнаяСтруктура(лг_НастройкаВызовСервера.Умолчание(Регион));
	
КонецФункции // Умолчание 

// Возвращает коллекцию настроек региона
//
// Параметры: 
// 	Регион - Строка - Регион использования
//
// Возвращаемое значение: 
// 	Соответствие 
//
Функция Коллекция(Регион = Неопределено) Экспорт
	
	Результат	= лг_НастройкаВызовСервера.Коллекция(Регион);
	
	Возврат ?(Результат = Неопределено
	, Результат
	, лг_Модуль.ом_Коллекция().ОбычнаяВФиксированная(Результат));
	
КонецФункции // Коллекция 

// Возвращает вид адаптеров хранения настроек логирования
//
// Параметры: 
//
// Возвращаемое значение: 
// 	Строка
//
Функция АдаптерВид() Экспорт
	
	Возврат "21e24ca0-a05c-4058-a277-d5f6bb35424b";
	
КонецФункции // АдаптерВид 

#КонецОбласти
